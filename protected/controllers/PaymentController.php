<?php

class PaymentController extends Controller{
	public $layout='//layouts/ind_payment';

	public function actions()
	{
		return array(
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	public function actionIndex($PackageID){
		if(Yii::app()->user->isGuest){
			$this->redirect(array('site/error2'));
		}
		else{
			$user = IndUser::model()->findByAttributes(
				array("Username"=>Yii::app()->user->getId())
			);
			$order = IndOrder::model()->findByAttributes(
				array("OrderID"=>$user->OrderID)
			);
			if($user->OrderID != null && date("Y-m-d") < $order->DateExpired){
				$this->redirect(array('site/error2'));
			}	
		}
		$payment = new Payment;
		$user = IndUser::model()->findByAttributes(
				array("Username"=>Yii::app()->user->getId()
		));
		$PackageID = Yii::app()->getRequest()->getParam('PackageID');
		$IndPayment = IndPayment::model()->findAll();

		if(isset($_POST["Payment"])){
			$UserInput=$_POST["Payment"];

			// IF CREDIT CARD //
			if($_POST["Payment"]["paymentID"]==1){
				$exp = $_POST['Payment']['cardExp']['month'] . "/" . $_POST['Payment']['cardExp']['year'];

				if($_POST["Payment"]["cardName"]==""){
					$payment->addError("cardName", "Card Holder Name cannot be blank");
				}
				else if($_POST['Payment']['cardNum']==""){
					$payment->addError('cardNum', 'Card Number cannot be blank');
				}
				else if(!is_numeric($_POST['Payment']['cardExp']['month']) || !is_numeric($_POST['Payment']['cardExp']['year'])){
					$payment->addError('cardExp', 'Expiration date must be number');
				}
				else if(!is_numeric($_POST['Payment']['CVV'])){
					$payment->addError('CVV', 'CVV must be number');
				}
			}
			else{
				if($_POST["Payment"]["paymentID"]!=2 || $_POST["Payment"]["paymentID"]!=3){
					$payment->addError('paymentID', 'Please select your payment method');	
				}
			}

			$payment->pay($user->UserID, $PackageID, $UserInput);

			echo "
			<script>
				alert('Thanks for the payment. We will check at your payment really soon and your subscription will be activated then');
				window.location = 'index.php';
			</script>";

			//$this->redirect(array('site/index'));
		}
		
		return $this->render('index', array(
			'payment'=>$payment,
			'IndPayment'=>$IndPayment,
		));
	}
}

?>