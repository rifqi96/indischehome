<?php

class AdminController extends Controller
{
    public $layout='//layouts/admin';

    public function actionIndex()
    {
        $user = IndUser::model()->findByAttributes(
                array("Username"=>Yii::app()->user->getId()
        ));
        if(Yii::app()->user->isGuest){
            $this->redirect('index.php?r=admin/login');
        }
        if(!Yii::app()->user->isGuest && $user->UserLevel !== "admin"){
            echo "<h2>You are not allowed to access this page. Please return to <a href='index.php'>homepage</a>.</h2>";
        }
        else{
            $query = "SELECT u.UserID, u.Username, u.Status, o.OrderID, o.Status AS 'OrderStatus'
                    FROM ind_user u
                    LEFT JOIN ind_order o ON u.OrderID = o.OrderID";
            $AllUsers= Yii::app()->db->createCommand($query)->queryAll();

            //$AllUsers = IndUser::model()->findAll();
            $AllPackages = IndPackage::model()->findAll();
            $AllOrders = IndOrder::model()->findAll();
            $AllPayments = IndPayment::model()->findAll();
            $AllCameras = IndCamera::model()->findAll();
            $this->render('index', array(
                'user' => $user,
                'AllUsers' => $AllUsers,
                'AllOrders' => $AllOrders,
                'AllPayments' => $AllPayments,
                'AllCameras' => $AllCameras,
            ));
        }
    }

    public function actionLogin(){
        $user = IndUser::model()->findByAttributes(
                array("Username"=>Yii::app()->user->getId()
        ));
        if(!Yii::app()->user->isGuest && $user->UserLevel !== "admin"){
            echo "<h2>You are not allowed to access this page. Please return to <a href='index.php'>homepage</a>.</h2>";
        }
        else{
            $loginModel=new LoginForm;

            // if it is ajax validation request
            if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
            {
                echo CActiveForm::validate($loginModel);
                Yii::app()->end();
            }

            // collect user input data
            if(isset($_POST['LoginForm']))
            {
                $loginModel->attributes=$_POST['LoginForm'];
                // validate user input and redirect to the previous page if valid
                if($loginModel->validate() && $loginModel->login()){
                    $user = IndUser::model()->findByAttributes(
                        array("Username"=>Yii::app()->user->getId()
                    ));
                    if($user->UserLevel !== "admin"){
                        echo "<script>alert('You are not allowed to use this page!');</script>";
                        Yii::app()->user->logout();
                    }
                    $this->redirect("index.php?r=admin/index");
                }
            }

            $this->render('login', array(
                "loginModel"=>$loginModel
            ));
        }
    }

    public function actionUpdateIP($id){
        $camera = IndCamera::model()->findByPk($id);

        $camera->attributes = $_POST['IndCamera'];

        if($camera->save()){
            Yii::app()->user->setFlash('success', "Update Succeed!");
            $this->redirect('index.php?r=admin/index');
        }
    }

    public function actionActivateOrder($id){
        $order = IndOrder::model()->findByPk($id);

        if($order->Status=="paid"){
            $order->Status = "confirmed";
        }
        else{
            $order->Status = "paid";
        }

        if($order->save()){
            Yii::app()->user->setFlash('success', "Update Succeed!");
            $this->redirect('index.php?r=admin/index');    
        }
    }

    // Uncomment the following methods and override them if needed
    /*
    public function filters()
    {
        // return the filter configuration for this controller, e.g.:
        return array(
            'inlineFilterName',
            array(
                'class'=>'path.to.FilterClass',
                'propertyName'=>'propertyValue',
            ),
        );
    }

    public function actions()
    {
        // return external action classes, e.g.:
        return array(
            'action1'=>'path.to.ActionClass',
            'action2'=>array(
                'class'=>'path.to.AnotherActionClass',
                'propertyName'=>'propertyValue',
            ),
        );
    }
    */
}