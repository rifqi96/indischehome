<?php

class Payment extends CFormModel
{
	
	public $paymentID;

	public $cardName;
	public $cardNum;
	public $cardExp;
	public $CVV;

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'cardName'=>'NAME ON CARD',
			'cardNum'=>'CARD NUMBER',
			'cardExp'=>'EXPIRATION',
			'CVV'=>'CVV NUMBER',
		);
	}

	public function pay($UserID, $PackageID, $UserInput){
		$user = IndUser::model()->findByPk($UserID);
		$package = IndPackage::model()->findByPk($PackageID);
		if($user->OrderID == null){
			$order = new IndOrder;
		}
		else{
			$order = IndOrder::model()->findByAttributes(
				array("OrderID" => $user->OrderID)
			);
		}
		$IndOrder = IndOrder::model()->findAll();
		$OrderID = $IndOrder[count($IndOrder)-1]->OrderID+1;

		$DateCreated = date("Y-m-d");
		$DateExpired = strtotime("+1 month", strtotime($DateCreated));
		$DateExpired = date("Y-m-d", $DateExpired);

		//$order->OrderID = $OrderID;
		$order->PackageID = $PackageID;
		$order->DateCreated = $DateCreated;
		$order->DateExpired = $DateExpired;
		$order->PaymentID = $UserInput['paymentID'];
		$order->Status = "confirmed";
		$order->save();
		
		$user->OrderID = $order->OrderID;
		$user->save();
	}
}
