<style>
a{
	color: #fff;
}

body{
	color: #fff;
}
table, td, th {    
    border: 1px solid #ddd;
    text-align: left;
}

table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 15px;
}

</style>
<h1>Admin Control Panel</h1>
<h1 style='color:green;'>
<?php if(Yii::app()->user->hasFlash('success')):?>

  <!-- <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>  -->
  <?php echo Yii::app()->user->getFlash('success'); ?>
    
<?php endif; ?>
</h1>
<a href="index.php?r=site/logout"><button>Logout</button></a>
<h1>User List</h1>

<table>
	<th>User ID</th>
	<th>Order ID</th>
	<th>Username</th>
	<th>Account Status</th>
	<th>Order Status</th>

	<?php
	for($i=0; $i<count($AllUsers); $i++){
		$OrderStatus="";
		if($AllUsers[$i]['OrderStatus'] === "confirmed"){
			$OrderStatus = "<a href='index.php?r=admin/ActivateOrder&id=".$AllUsers[$i]['OrderID']."'>Confirmed</a>";
		}
		else{
			$OrderStatus = "<a href='index.php?r=admin/ActivateOrder&id=".$AllUsers[$i]['OrderID']."'>Paid</a>";
		}

		echo "<tr><td>".$AllUsers[$i]['UserID']."</td><td>".$AllUsers[$i]['OrderID']."</td><td>".$AllUsers[$i]['Username']."</td><td>".$AllUsers[$i]['Status']."<td>".$OrderStatus."</td></tr>";
	}
	?>
</table>

<h1>Camera IP</h1>
<table>
	<th>Camera ID</th>
	<th>IP</th>
	<th>Action</th>
	<?php
	foreach($AllCameras as $c){
		echo "<tr><td>".$c->CameraID."</td><td>".$c->Source."</td><td><form action='index.php?r=admin/updateIP&id=".$c->CameraID."' method='POST'><input type='text' name='IndCamera[Source]'><button class='btn btn-primary btn-block btn-large'>Update</button></form></tr>";
	}
	?>
</table>