<style>
body{
	color: #fff;
}
</style>
<h1>Admin CP Login</h1>

<?php $form=$this->beginWidget('CActiveForm', array(
'id'=>'login-form',
'enableClientValidation'=>true,
'clientOptions'=>array(
  'validateOnSubmit'=>true,
),
)); ?>
<?php echo $form->textField($loginModel,'username', array('class'=>'form-control', 'placeholder'=>'Input your username', 'id'=>'input-username')); ?>
<?php echo $form->error($loginModel,'username'); ?>
<!-- <input type="email" class="form-control" placeholder="email" id="input-email" name="email"/> -->

<?php echo $form->passwordField($loginModel,'password', array('class'=>'form-control', 'placeholder'=>'Input your password', 'id'=>'input-password')); ?>
<?php echo $form->error($loginModel,'password'); ?>
<!-- <input type="password" class="form-control" placeholder="password" id="input-password" name="password"/> -->

<button type="submit" class="btn btn-primary btn-block btn-large">Let me in.</button>
<?php $this->endWidget();
?>