<div class="sap_tabs">
	<div id="horizontalTab" style="display: block; width: 100%; margin: 0px;">
		<div class="pay-tabs">
			<h2>Select Payment Method</h2>
			  <ul class="resp-tabs-list">
				<li class="resp-tab-item" aria-controls="tab_item-1" role="tab"><span><label class="pic1"></label>Credit Card</span></li>
				<li class="resp-tab-item" aria-controls="tab_item-1" role="tab"><span><label class="pic3"></label>Net Banking</span></li>
				<!-- <li class="resp-tab-item" aria-controls="tab_item-2" role="tab"><span><label class="pic4"></label>PayPal</span></li>  -->
				<!-- <li class="resp-tab-item" aria-controls="tab_item-3" role="tab"><span><label class="pic2"></label>Debit Card</span></li> -->
				<div class="clear"><button onclick="back();">BACK</button></div>
			  </ul>
			  <script type="text/javascript">
			  	function back(){
			  		window.location="index.php";
			  	}
			  </script>	
		</div>
		<div class="resp-tabs-container">
			<div class="tab-1 resp-tab-content" aria-labelledby="tab_item-0">
				<div class="payment-info">
					<!-- <h3>Personal Information</h3>
					<form>
						<div class="tab-for">				
							<h5>EMAIL ADDRESS</h5>
								<input type="text" value="">
							<h5>FIRST NAME</h5>													
								<input type="text" value="">
						</div>			
					</form> -->
					<h3 class="pay-title">Credit Card Info</h3>
					<?php
					$form=$this->beginWidget('CActiveForm', array(
						'id'=>'ind-order-credit-form',
						'enableClientValidation'=>true,
						    'clientOptions'=>array(
						      'validateOnSubmit'=>true,
						      ),
						// Please note: When you enable ajax validation, make sure the corresponding
						// controller action is handling ajax validation correctly.
						// See class documentation of CActiveForm for details on this,
						// you need to use the performAjaxValidation()-method described there.
						'enableAjaxValidation'=>false,
					));
					?>
						<div class="tab-for">				
						<center><span style="color:red;"><?php echo $form->errorSummary($payment); ?></span></center>
							<h5><?php echo $form->labelEx($payment,'cardName'); ?></h5>
								<?php echo $form->textField($payment,'cardName', array('required'=>'true')); ?>
					            <?php echo $form->error($payment,'cardName'); ?>
							<h5><?php echo $form->labelEx($payment,'cardNum'); ?></h5>
								<?php echo $form->textField($payment,'cardNum', array('class'=>'pay-logo', 'placeholder'=>'0000-0000-0000-0000', 'required'=>'true')); ?>
					            <?php echo $form->error($payment,'cardNum'); ?>	
						</div>	
						<div class="transaction">
							<div class="tab-form-left user-form">
								<h5><?php echo $form->labelEx($payment,'cardExp'); ?></h5>
									<ul>
										<li>
											<input type="number" name="Payment[cardExp][month]" class='text_box' value='6' min='1' required='true'>
					            			<?php echo $form->error($payment,'cardExp'); ?>	
										</li>
										<li>
											<input type="number" name="Payment[cardExp][year]" class='text_box' value='1988' min='1' required='true'>
					            			<?php echo $form->error($payment,'cardExp'); ?>
										</li>
										
									</ul>
							</div>
							<div class="tab-form-right user-form-rt">
								<h5><?php echo $form->labelEx($payment,'CVV'); ?></h5>
								<?php echo $form->textField($payment,'CVV', array('required'=>'true', 'placeholder'=>'xxxx')); ?>
					            <?php echo $form->error($payment,'CVV'); ?>
							</div>
							<div class="clear"></div>
						</div>
						<div class="single-bottom">
							<ul>
								<li>
									<input type="checkbox" id="agree1" name="agree1" value="1" required="true">
									<label for="agree1"><span></span>By checking this box, I agree to the Terms & Conditions & Privacy Policy.</label>
								</li>
							</ul>
						</div>
						<?php echo $form->hiddenField($payment, 'paymentID', array('value'=>'1')) ?>
						<input type="submit" value="P A Y">
					<?php
					$this->endWidget();
					?>
				</div>
			</div>
			<div class="tab-1 resp-tab-content" aria-labelledby="tab_item-1">
				<div class="payment-info">
					<h3 class="pay-title">Net Banking</h3>
					<center><span style="color:red;"><?php echo $form->errorSummary($payment); ?></span></center>
					<?php
					$form=$this->beginWidget('CActiveForm', array(
						'id'=>'ind-order-bank-form',
						'enableClientValidation'=>true,
						    'clientOptions'=>array(
						      'validateOnSubmit'=>true,
						      ),
						// Please note: When you enable ajax validation, make sure the corresponding
						// controller action is handling ajax validation correctly.
						// See class documentation of CActiveForm for details on this,
						// you need to use the performAjaxValidation()-method described there.
						'enableAjaxValidation'=>false,
					));
					?>
						<input type="radio" name="Payment[paymentID]" value="2" id="bankislam"> <label for="bankislam"><img src="<?php echo Yii::app()->request->baseUrl . '/assets/payment_bankislam.jpg' ?>" width="75" height="75"> 	</label><br>
						<input type="radio" name="Payment[paymentID]" value="3" id="cimb"> <label for="cimb"><img src="<?php echo Yii::app()->request->baseUrl . '/assets/payment_cimb.png' ?>" width="75" height="75">	</label><br>

						<div class="well" id="bankislam-div" style="display:none;">
							<h3>Please complete your payment to:</h3><br>
							<h3>Bank Islam</h3>
							<h3>Account Number: 102938129389</h3>
							<h3>Account Name: Indische Home</h3>
						</div>

						<div class="well" id="cimb-div" style="display:none;">
							<h3>Please complete your payment to:</h3><br>
							<h3>CIMB</h3>
							<h3>Account Number: 009213882731</h3>
							<h3>Account Name: Indische Home</h3>
						</div>

						<script>
							$(document).ready(function() {
							  $('input').change(function() {
							    if ($('input[id="bankislam"]').is(':checked')) {
							      $('#bankislam-div').show();
							    }
							    else {
							      $('#bankislam-div').hide();
							    }

							    if($('input[id="cimb"]').is(':checked')){
							    	$('#cimb-div').show();
							    }
							    else{
							    	$('#cimb-div').hide();
							    }
							  });
							});
						</script>

						<style type="text/css">
							.well{
								background: #d6d6c2;
							}
						</style>

						<ul>
							<li>
								<input type="checkbox" name="agree" value="1" id="agree2" required="true">
								<label for="agree2"> By checking this box, I agree to the Terms & Conditions & Privacy Policy.</label>
							</li>
						</ul>
						<input type="submit" value="P A Y">
					<?php
					$this->endWidget();
					?>
					<div class="clear"></div>
					</div>
				</div>
			</div>
			<!-- <div class="tab-1 resp-tab-content" aria-labelledby="tab_item-2">
				<div class="payment-info">
					<h3>PayPal</h3>
					<h4>Already Have A PayPal Account?</h4>
					<div class="login-tab">
						<div class="user-login">
							<h2>Login</h2>
							
							<form>
								<input type="text" value="name@email.com" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'name@email.com';}" required="">
								<input type="password" value="PASSWORD" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'PASSWORD';}" required="">
									<div class="user-grids">
										<div class="user-left">
											<div class="single-bottom">
													<ul>
														<li>
															<input type="checkbox"  id="brand1" value="">
															<label for="brand1"><span></span>Remember me?</label>
														</li>
													</ul>
											</div>
										</div>
										<div class="user-right">
											<input type="submit" value="LOGIN">
										</div>
										<div class="clear"></div>
									</div>
							</form>
						</div>
					</div>
				</div>
			</div> -->
			<!-- <div class="tab-1 resp-tab-content" aria-labelledby="tab_item-3">	
				<div class="payment-info">
					
					<h3 class="pay-title">Dedit Card Info</h3>
					<form>
						<div class="tab-for">				
							<h5>NAME ON CARD</h5>
								<input type="text" value="">
							<h5>CARD NUMBER</h5>													
								<input class="pay-logo" type="text" value="0000-0000-0000-0000" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = '0000-0000-0000-0000';}" required="">
						</div>	
						<div class="transaction">
							<div class="tab-form-left user-form">
								<h5>EXPIRATION</h5>
									<ul>
										<li>
											<input type="number" class="text_box" type="text" value="6" min="1" />	
										</li>
										<li>
											<input type="number" class="text_box" type="text" value="1988" min="1" />	
										</li>
										
									</ul>
							</div>
							<div class="tab-form-right user-form-rt">
								<h5>CVV NUMBER</h5>													
								<input type="text" value="xxxx" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'xxxx';}" required="">
							</div>
							<div class="clear"></div>
						</div>
						<input type="submit" value="SUBMIT">
					</form>
					<div class="single-bottom">
							<ul>
								<li>
									<input type="checkbox"  id="brand" value="">
									<label for="brand"><span></span>By checking this box, I agree to the Terms & Conditions & Privacy Policy.</label>
								</li>
							</ul>
					</div>
				</div>	
			</div> -->
		</div>	
	</div>
</div>